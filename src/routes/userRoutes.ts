import { Router } from "express";
import { getUserByParams } from "../controllers/userController";

const router = Router();

router.post("/login/", getUserByParams);

export default router;
