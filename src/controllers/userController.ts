import { RequestHandler } from "express";
import dbConnection from "../utils/dbConnection";
import { passValidation } from "../utils/passValidation";

import { UsersTable } from "../models/userModel";

export const getUserByParams: RequestHandler = async (req, res) => {
  try {
    const { username, password } = req.query;

    if (!username || !password) {
      return res.status(400).json({
        error: "username and password are required",
      });
    }

    const [user] = await dbConnection.query<UsersTable[]>(
      "SELECT * FROM Users WHERE username = ?",
      [username]
    );

    if (!user.length) {
      return res.status(404).json({
        message: "User not found",
      });
    }

    const verification: boolean = await passValidation(
      password as string,
      user[0].password
    );

    if (!verification) {
      res.status(401).json({
        message: "Password incorrect",
      });
    } else {
      return res.status(200).json({
        message: "The user was found",
        fullName: user[0].fullname,
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: error,
    });
  }
};
