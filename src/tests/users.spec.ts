import app from "../app";
import supertest from "supertest";

const request = supertest(app);

jest.setTimeout(10000);

describe("/POST /api/users/login", () => {
  it("should return a 200 response", async () => {
    const res = await request.post("/api/users/login/").query({
      username: "admin",
      password: "admin",
    });
    expect(res.status).toBe(200);
    expect(res.body.message).toBe("The user was found");
  });
  it("should return a 400 for missing data", async () => {
    const res = await request.post("/api/users/login/");
    expect(res.status).toBe(400);
  });
  it("should return a 401 for incorrect password", async () => {
    const res = await request.post("/api/users/login/").query({
      username: "admin",
      password: "admin1",
    });
    expect(res.status).toBe(401);
    expect(res.body.message).toBe("Password incorrect");
  });
  it("should return a 404 for incorrect username", async () => {
    const res = await request.post("/api/users/login/").query({
      username: "testing",
      password: "admin",
    });
    expect(res.status).toBe(404);
    expect(res.body.message).toBe("User not found");
  });
});
