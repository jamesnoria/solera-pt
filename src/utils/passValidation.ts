import * as bycript from "bcrypt";

export const passValidation = (
  password: string,
  hash: string
): Promise<boolean> => {
  return bycript.compare(password, hash);
};
