import { RowDataPacket } from "mysql2";

export interface UsersTable extends RowDataPacket {
  id: number;
  user: string;
  password: string;
  fullName: string;
  createdAt: Date;
  updatedAt: Date;
}
