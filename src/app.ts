import express from "express";
import { json } from "body-parser";
import morgan from "morgan";

import userRoutes from "./routes/userRoutes";

const app = express();

app.use(json());
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));

app.use("/api/users", userRoutes);

export default app;
