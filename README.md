# RestApi - Prueba Técnica para SOLERA

![flow](flow.jpeg)

## Documentación

Puede encontrar más detalle sobre esta API en el siguiente enlace: [DOCS](https://documenter.getpostman.com/view/21763436/UzJFuyE8)

## Instalación

- Clonar este repositorio
- Instalar dependencias:
    ```
    npm install
    ```
- Remplazar las variables de entorno según `.env-example` por sus valores correctos o necesarios para la ejecución del programa.

- Ejecutar el servidor:
    ```
    npm run dev
    ```
- Obtener respuesta (atraves de un cliente HTTP):
    ```
    localhost:3000/api/users/login/?username=admin&password=admin
    ```

## Ejecutando las pruebas

- Puede correr el siguiente comando para ejecutar los tests:
    ```
    npm run test
    ```

## Integración Continua

- Por cada push a la rama `main` se ejecutan los tests automaticamente. Ver `gitlab-ci.yml`

## Construido con

* Typescript
* NodeJS - Express
* Mysql2 (Base de datos)
* Jest (Pruebas)